package ir.narangsoft.navigation_view2;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


public class ContentFragment extends Fragment  {

    @Nullable

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.content_fragment,container,false);

        return v;
    }
}


@SuppressLint("ValidFragment")
class ContentFragment2 extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_fragment2,container,false);
        return v;
    }
}

@SuppressLint("ValidFragment")
class ContentFragment3 extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_fragment3,container,false);
        return v;
    }
}

@SuppressLint("ValidFragment")
class ContentFragment4 extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_fragment4,container,false);
        return v;
    }
}

@SuppressLint("ValidFragment")
class ContentFragment5 extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_fragment5,container,false);
        return v;
    }
}

@SuppressLint("ValidFragment")
class ContentFragment6 extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_fragment6,container,false);
        return v;
    }
}
@SuppressLint("ValidFragment")
class ContentFragment7 extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_fragment7,container,false);
        return v;
    }
}

@SuppressLint("ValidFragment")
class ContentFragment8 extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_fragment8,container,false);
        return v;
    }

}


//compile 'com.android.support:appcompat-v7:23.1.2'